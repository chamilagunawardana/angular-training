﻿angular.module('detail').directive('movieRate', movieRate);

function movieRate() {
    return {
        restrict: 'AE',
        scope: {
            rate: '='
        },
        //template: '<ul class="rating">' +
        //    '<li ng-repeat="star in stars" ng-class="star">' +
        //    '\u2605' +
        //    '</li>' +
        //    '</ul>',     
        templateUrl: 'detail/movieRate.html',
        link: function (scope, elem, attrs) {
            scope.stars = [];
            for (var i = 0; i < scope.rate; i++) {
                scope.stars.push({i                    
                });
            }
        }
    };
}