﻿angular.module('detail').controller('MovieDetailController', MovieDetailController);

function MovieDetailController(DataService, $stateParams) {
    var vm = this;    
    init();

    function init() {
        DataService.getMovieCollection().then(function (data) {
            var movieId = $stateParams.id;

            vm.movieId = movieId;
            data.forEach(function (movie) {
                if (+movie.id == +movieId) {
                    vm.movie = movie;
                }
            });
        });
    }
}