﻿angular.module('createMovie').controller('CreateController', CreateMovieController);

function CreateMovieController(DataService) {
    var vm = this;
    vm.isMovieAdded = false;
    vm.title = 'Create movie';
    vm.characterCollection = [];

    vm.save = function () {
        vm.isMovieAdded = true;
    };

    vm.add = function () {
        var characterName = vm.characterName;
        var actor = vm.actorName;

        vm.characterName = "";
        vm.actorName = "";

        vm.characterCollection.push({
            charactor: characterName,
            actor: actor
        });
    }
}