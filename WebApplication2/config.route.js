﻿angular.module('app').config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('createMovie', {
        url: '/create',
        templateUrl: 'create/createMovie.html',
        controller: 'CreateController as movieCreate'
    }).state('index', {
        url: '/list',
        templateUrl: 'list/movieList.html',
        controller: 'ListMovieController as movieList'
    }).state('list', {
        url: '/list',
        templateUrl: 'list/movieList.html',
        controller: 'ListMovieController as movieList'
    }).state('detail', {
        url: '/detail/:id',
        templateUrl: 'detail/movieDetail.html',
        controller: 'MovieDetailController as movieDetail'
    });
});