﻿angular.module('list').controller('ListMovieController', ListMovieController);


function ListMovieController(DataService) {
    var vm = this;
    init();
    function init() {
        DataService.getMovieCollection().then(function (data) {
            vm.movieCollection = data;
        });
    }
    //vm.title = 'Hello World';

    // $scope.title = 'Hello World';
}