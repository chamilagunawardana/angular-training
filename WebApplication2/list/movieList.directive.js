﻿angular.module('list').directive('movieItem', movieItem);

function movieItem() {
    return {
        restrict: 'AE',
        scope: {
            movie: '='
        },
        template: `
      <div movie-rate rate="movie.rating"></div>
      <h3>{{movie.movieName}}</h3>      
      <p>{{movie.movieDescription}}</p>      
      <a ui-sref="detail({ id: movie.id })">Detail</a>        
    `
    };
}