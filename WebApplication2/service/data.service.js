﻿angular.module('app').service('DataService', DataService);

function DataService($http) {
    var self = this;

    self.getMovieCollection = function () {
        return $http.get('service/movies.json').then(function (response) {
            var movieCollection = response.data;
            return movieCollection;
        });
    }
}